#include <stdio.h>

enum file_open_status  {
    FILE_OPEN_SUCCESS = 0,
    FILE_OPEN_FAILURE,
};

enum file_close_status {
    FILE_CLOSE_SUCCESS = 0,
    FILE_CLOSE_FAILURE,
    };

//enum file_open_status open(FILE** const file, char const * const file_name, char * const mode);
enum file_close_status close(FILE* file_name);


