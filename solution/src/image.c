#include "image.h"
//#include <malloc.h>
#include "stdlib.h"

struct image image_create(uint64_t const width, uint64_t const height){
    struct image img = {
            .width = width,
            .height = height,
            .data = malloc(3 * width * height)
    };
    return img;

}
void image_destroy(struct image* const image){
    free(image->data);
}

struct pixel get_pixel(struct image const* img ,uint64_t const offset, uint64_t const row){
    struct pixel res = img->data[img->width * row + offset];
    return res;
}
void set_pixel(struct image* img ,uint64_t const offset, uint64_t const row ,struct pixel px){
    img->data[img->width*row + offset] = px;
}
