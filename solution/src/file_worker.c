#include "file_worker.h"


//enum file_open_status open(FILE** const file, char const * const file_name, char * const mode_for_open){
//    *file = fopen(file_name, mode_for_open);
//    return (((*file))== NULL ? FILE_OPEN_FAILURE : FILE_OPEN_SUCCESS);
//}


enum file_close_status close(FILE* file_name){
    return ((fclose(file_name)) ? FILE_CLOSE_FAILURE : FILE_CLOSE_SUCCESS);
}
