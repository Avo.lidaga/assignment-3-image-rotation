#include "../include/bmp.h"
//#include <malloc.h>
#include "stdlib.h"

#define ZERO 0
#define OFFSET 54
#define SIGNATURE 0x4D42
#define SIZE 40
#define PLANES 1
#define BIT_COUNT 24

static struct bmp_header create_header(uint64_t const width, uint64_t const height){
    uint64_t img_size = sizeof(struct pixel) * width *height;
    return (struct bmp_header) {
            .bfType = SIGNATURE,
            .bfileSize = OFFSET + img_size,
            .bfReserved = ZERO,
            .bOffBits = OFFSET,
            .biSize = SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = ZERO,
            .biSizeImage = img_size,
            .biXPelsPerMeter = ZERO,
            .biYPelsPerMeter = ZERO,
            .biClrUsed = ZERO,
            .biClrImportant = ZERO
    };
}

static inline uint8_t calculate_padding(uint64_t const width){
    return (width % 4 == 0) ? 0 : (4 - (width * sizeof(struct pixel)) % 4);
}

static enum read_status validate_header(struct bmp_header const * const header){
    if (header->bfType!=SIGNATURE) return READ_INVALID_SIGNATURE;
    if (header->biBitCount != BIT_COUNT) return READ_INVALID_HEADER;
    if (header->biSize != SIZE) return READ_INVALID_SIZE;
    if (header->biPlanes != PLANES) return READ_INVALID_PLANES;
    return READ_OK;
}



enum read_status from_bmp( FILE* const in, struct image* const img) {
    struct bmp_header header = {0};
    fread(&header, sizeof( struct bmp_header ), 1, in );
    if ((validate_header(&header) == READ_OK ?  READ_OK : READ_INVALID_HEADER) != READ_OK) return READ_INVALID_HEADER;

    *img = image_create(header.biWidth,header.biHeight);

    for (uint32_t i = 0; i < img->height; i++){
        if (!fread(img->data + i * img->width, img->width * sizeof(struct pixel), 1, in)){
            return READ_INVALID_BITS;
        }
        if (fseek(in, calculate_padding(img->width ), SEEK_CUR)){
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}


static enum write_status write_header(FILE* const out, struct bmp_header const * const header){
    if (header != NULL){
        if (fwrite(header, sizeof(struct bmp_header), 1,out)==1) return WRITE_OK;
    }
    return WRITE_ERROR;
}

static enum write_status write_data(FILE* const out, struct image const* const img){
    if (img->data != NULL) {
        uint8_t padding = calculate_padding(img->width);
        for (size_t i = 0; i < img->height; i++) {
            if (fwrite(img->data + img->width * i, sizeof(struct pixel), img->width, out) != img->width
                    || fseek(out, padding, SEEK_CUR))
                return WRITE_ERROR;
        }
        return WRITE_OK;
    }
    return WRITE_ERROR;
}


enum write_status to_bmp( FILE* const out, struct image const* const img ){
    struct bmp_header header = create_header(img->width, img->height);
    if (write_header(out, &header)!= WRITE_OK || write_data(out, img)!=WRITE_OK) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}
