#include "image.h"
#include "../include/rotate.h"
//#include <malloc.h>
#include "stdlib.h"

struct image rotate (struct image *img) {



    struct image  new_image  =  image_create ( img->height, img->width);

    for (size_t i = 0; i < img->width; i++) {
        for (size_t j = 0; j < img->height; j++) {

            set_pixel(&new_image,img->height - j - 1,i,get_pixel(img,i,j));

        }
    }
    return new_image;
}
