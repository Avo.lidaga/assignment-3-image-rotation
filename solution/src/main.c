#include "../include/file_worker.h"
#include "../include/rotate.h"
#include "bmp.h"

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        printf("Incorrect number of params... Sorry\n");
        return 1;
    }
    else
    {
        FILE *in = fopen(argv[1], "rb");
        FILE *out = fopen(argv[2], "wb");

        if (in == NULL || out == NULL)
        {
            fprintf(stderr, "Unable to open file\n");
            return 1;
        }
        struct image img = {0};

        if (from_bmp(in, &img) != READ_OK)
        {
            fprintf(stderr, "Unable to read input file\n");
            close(in);
            close(out);
            return 1;
        }

        struct image rotated = rotate(&img);
        if (to_bmp(out, &rotated) == WRITE_ERROR)
        {
            fprintf(stderr, "Unable to read output file\n");
            close(in);
            close(out);
            return 1;
        }
        image_destroy(&img);
        image_destroy(&rotated);
        return 0;
    }
}
